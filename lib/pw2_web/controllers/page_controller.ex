defmodule Pw2Web.PageController do
  use Pw2Web, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
